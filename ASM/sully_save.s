_itoa :
	push rdi				; save nb
	mov rcx, rdi
	mov rdi, 2
	cmp rcx, 9
	jnb _add_len
	jmp _itoa_next

_add_len :
	inc rdi

_itoa_next :
	push rdi				; save len
	call _malloc
	pop rdi					; recup len
	mov rsi, rdi
	push rdi				; save len
	mov rdi, rax
	call _bzero
	pop rdi					; recup len
	dec rdi
	pop rcx					; recup nb 
	cmp rcx, 0				; cmp nb 0
	je _case_zero
	mov r8, rax				; str ret

_loop_nb :					; rcx = nb ; rdi = len
	cmp rcx, 0
	je _itoa_end

	mov rdx, 0
	mov rax, rcx
	mov r12, 10
	div r12

	;mov rsi, rdi
	;lea rdi, [rel debug_int]
	;call _printf

	add rdx, 48
	mov [r8 + rdi], rdx
	dec rdi

	mov rcx, rax

	jmp _loop_nb

_itoa_end :
	mov rsi, r8
	lea rdi,[rel debug_str]
	call _printf
	ret

_case_zero :
	mov byte[rax], 48
	jmp _itoa_end