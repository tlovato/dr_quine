; Commentaire 1

section .data
	all : dd "; Commentaire 1%1$c%1$csection .data%1$c%2$call : dd %3$c%4$s%3$c, 0%1$c%1$csection .text%1$c%2$cglobal _main%1$c%2$cextern _printf%1$c%1$c_main :%1$c%2$c; Commentaire 2%1$c%2$cpush rbp%1$c%2$cmov rbp, rsp%1$c%2$ccall _function%1$c%2$clea rdi, [rel all]%1$c%2$cmov rsi, 10%1$c%2$cmov rdx, 9%1$c%2$cmov rcx, 34%1$c%2$clea r8, [rel all]%1$c%2$ccall _printf%1$c%2$cleave%1$c%2$cret%1$c%1$c_function :%1$c%2$cret%1$c", 0

section .text
	global _main
	extern _printf

_main :
	; Commentaire 2
	push rbp
	mov rbp, rsp
	call _function
	lea rdi, [rel all]
	mov rsi, 10
	mov rdx, 9
	mov rcx, 34
	lea r8, [rel all]
	call _printf
	leave
	ret

_function :
	ret
