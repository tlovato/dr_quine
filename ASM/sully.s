section .data
	base_name : db "Sully_", 0
	ext : db ".s", 0
	debug_int : db "%d", 10, 0
	debug_str : db "%s", 10, 0

section .text
	global _main
	extern _malloc
	extern _bzero
	extern _strcpy

	extern _printf
	
_main : 
	push rbp
	mov rbp, rsp
	mov rcx, 25					; int i

	mov rdi, rcx
	mov rsi, 0
	call _get_name

	leave
	ret

_get_name :
	push rbp
	mov rbp, rsp

	push rax					; alignement
	push rdi					; = int i
	mov rdi, 11
	call _malloc
	mov rdi, rax
	mov rsi, 11
	call _bzero
	mov rdi, rax
	lea rsi, [rel base_name]
	call _strcpy
	pop rdi						; = int nb
	mov rcx, 0					; compteur pour len
	call _len

_get_name_after_len :
	;mov rsi, rax
	;call _itoa
	leave
	ret

_len :
	cmp rdi, 0
	je _get_name_after_len
	mov rdx, 0
	mov rax, rdi
	mov r12, 10
	div r12

	mov rsi, rax
	lea rdi, [rel debug_int]
	call _printf

	mov rdi, rax
	inc rcx
	jmp _len

_itoa :
	ret

