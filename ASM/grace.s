%define NO_MAIN _main :
%define STR "%%define NO_MAIN _main :%1$c%%define STR %3$c%4$s%3$c, 0%1$c%%define KID %3$cGrace_kid.s%3$c, 0%1$c%1$c; Commentaire%1$c%1$csection .data%1$c%2$call : db STR%1$c%2$ckid : db KID%1$c%1$csection .text%1$c%2$cglobal _main%1$c%2$cextern _dprintf%1$c%1$cNO_MAIN%1$c%2$cpush rbp%1$c%2$cmov rbp, rsp%1$c%2$clea rdi, [rel kid]%1$c%2$cmov rsi, 0x0002 | 0x0200 | 0x0400%1$c%2$cmov rdx, 0x888 | 0x777%1$c%2$cmov rax, 0x2000005%1$c%2$csyscall%1$c%2$cmov rdi, rax%1$c%2$clea rsi, [rel all]%1$c%2$cmov rdx, 10%1$c%2$cmov rcx, 9%1$c%2$cmov r8, 34%1$c%2$clea r9, [rel all]%1$c%2$ccall _dprintf%1$c%2$cmov rax, 0x2000006%1$c%2$csyscall%1$c%2$cleave%1$c%2$cret%1$c", 0
%define KID "Grace_kid.s", 0

; Commentaire

section .data
	all : db STR
	kid : db KID

section .text
	global _main
	extern _dprintf

NO_MAIN
	push rbp
	mov rbp, rsp
	lea rdi, [rel kid]
	mov rsi, 0x0002 | 0x0200 | 0x0400
	mov rdx, 0x888 | 0x777
	mov rax, 0x2000005
	syscall
	mov rdi, rax
	lea rsi, [rel all]
	mov rdx, 10
	mov rcx, 9
	mov r8, 34
	lea r9, [rel all]
	call _dprintf
	mov rax, 0x2000006
	syscall
	leave
	ret
