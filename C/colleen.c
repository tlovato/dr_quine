#include <stdio.h>

/*
	Commentaire 1
*/

void function(void) {};

int main()
{
/*
	Commentaire 2
*/
char *str = "#include <stdio.h>%1$c%1$c/*%1$c%3$cCommentaire 1%1$c*/%1$c%1$cvoid function(void) {};%1$c%1$cint main()%1$c{%1$c/*%1$c%3$cCommentaire 2%1$c*/%1$cchar *str = %2$c%4$s%2$c;%1$cfunction();%1$cprintf(str, 10, 34, 9, str);%1$creturn (0);%1$c}%1$c";
function();
printf(str, 10, 34, 9, str);
return (0);
}
