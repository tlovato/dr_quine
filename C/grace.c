#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

/*
	Commentaire
*/

#define MAIN(contents) int main() {contents}
#define STR "#include <stdio.h>%1$c#include <fcntl.h>%1$c#include <unistd.h>%1$c%1$c/*%1$c%3$cCommentaire%1$c*/%1$c%1$c#define MAIN(contents) int main() {contents}%1$c#define STR %2$c%5$s%2$c%1$c#define CONTENT int fd = open(%2$c%4$s%2$c, O_RDWR | O_CREAT | O_TRUNC, S_IRWXU | S_IRWXG | S_IRWXO); char *str = STR; dprintf(fd, str, 10, 34, 9, %2$c%4$s%2$c, str); close(fd); return (0);%1$c%1$cMAIN(CONTENT);%1$c"
#define CONTENT int fd = open("Grace_kid.c", O_RDWR | O_CREAT | O_TRUNC, S_IRWXU | S_IRWXG | S_IRWXO); char *str = STR; dprintf(fd, str, 10, 34, 9, "Grace_kid.c", str); close(fd); return (0);

MAIN(CONTENT);
