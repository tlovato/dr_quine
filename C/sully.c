#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

static long ft_intlen(long n)
{
	long len;

	len = 0;
	if (n < 0)
	{
		n = -n;
		len = 1;
	}
	if (n == 0)
		return (1);
	while (n > 0)
	{
		n = n / 10;
		len++;
	}
	return (len);
}

static char	*ft_core(long n, char *str)
{
	long len;

	len = ft_intlen(n);
	if (n < 0)
		n = -n;
	str[len] = 0;
	while (n != 0)
	{
		str[--len] = n % 10 + 48;
		n = n / 10;
	}
	return (str);
}

static char	*ft_itoa(int nb)
{
	char *str;
	int i;
	long n;

	if ((str = (char *)malloc(ft_intlen(nb) + 1 * (sizeof(char)))) == NULL)
		return (NULL);
	i = 0;
	n = nb;
	bzero(str, ft_intlen(nb));
	if (n == 0)
	{
		str[0] = '0';
		return (str);
	}
	if (n < 0)
		str[0] = '-';
	return (ft_core(n, str));
}

static char *get_name(int i, int exec)
{
	char *name;

	if (!(name = (char *)malloc(sizeof(char) * 11)))
		return (NULL);
	bzero(name, 11);
	strcpy(name, "Sully_");
	strcpy(&name[6], ft_itoa(i));
	if (!exec)
		strcpy(&name[(i > 9) ? 8 : 7], ".c");
	return (name);
}

static void write_in_file(char *name, int i)
{
	int fd = open(name, O_RDWR | O_CREAT | O_TRUNC, S_IRWXU | S_IRWXG | S_IRWXO);
	char *str = "#include <stdlib.h>%1$c#include <stdio.h>%1$c#include <strings.h>%1$c#include <unistd.h>%1$c#include <fcntl.h>%1$c#include <errno.h>%1$c%1$cstatic long ft_intlen(long n)%1$c{%1$c%3$clong len;%1$c%1$c%3$clen = 0;%1$c%3$cif (n < 0)%1$c%3$c{%1$c%3$c%3$cn = -n;%1$c%3$c%3$clen = 1;%1$c%3$c}%1$c%3$cif (n == 0)%1$c%3$c%3$creturn (1);%1$c%3$cwhile (n > 0)%1$c%3$c{%1$c%3$c%3$cn = n / 10;%1$c%3$c%3$clen++;%1$c%3$c}%1$c%3$creturn (len);%1$c}%1$c%1$cstatic char	*ft_core(long n, char *str)%1$c{%1$c%3$clong len;%1$c%1$c%3$clen = ft_intlen(n);%1$c%3$cif (n < 0)%1$c%3$c%3$cn = -n;%1$c%3$cstr[len] = 0;%1$c%3$cwhile (n != 0)%1$c%3$c{%1$c%3$c%3$cstr[--len] = n %% 10 + 48;%1$c%3$c%3$cn = n / 10;%1$c%3$c}%1$c%3$creturn (str);%1$c}%1$c%1$cstatic char	*ft_itoa(int nb)%1$c{%1$c%3$cchar *str;%1$c%3$cint i;%1$c%3$clong n;%1$c%1$c%3$cif ((str = (char *)malloc(ft_intlen(nb) + 1 * (sizeof(char)))) == NULL)%1$c%3$c%3$creturn (NULL);%1$c%3$ci = 0;%1$c%3$cn = nb;%1$c%3$cbzero(str, ft_intlen(nb));%1$c%3$cif (n == 0)%1$c%3$c{%1$c%3$c%3$cstr[0] = '0';%1$c%3$c%3$creturn (str);%1$c%3$c}%1$c%3$cif (n < 0)%1$c%3$c%3$cstr[0] = '-';%1$c%3$creturn (ft_core(n, str));%1$c}%1$c%1$cstatic char *get_name(int i, int exec)%1$c{%1$c%3$cchar *name;%1$c%1$c%3$cif (!(name = (char *)malloc(sizeof(char) * 11)))%1$c%3$c%3$creturn (NULL);%1$c%3$cbzero(name, 11);%1$c%3$cstrcpy(name, %2$cSully_%2$c);%1$c%3$cstrcpy(&name[6], ft_itoa(i));%1$c%3$cif (!exec)%1$c%3$c%3$cstrcpy(&name[(i > 9) ? 8 : 7], %2$c.c%2$c);%1$c%3$creturn (name);%1$c}%1$c%1$cstatic void write_in_file(char *name, int i)%1$c{%1$c%3$cint fd = open(name, O_RDWR | O_CREAT | O_TRUNC, S_IRWXU | S_IRWXG | S_IRWXO);%1$c%3$cchar *str = %2$c%5$s%2$c;%1$c%3$cdprintf(fd, str, 10, 34, 9, i, str);%1$c%3$cclose(fd);%1$c}%1$c%1$cstatic int fork_duplicate(char *name, char *name_exec, int i)%1$c{%1$c%3$cpid_t pid = fork();%1$c%3$cchar *args_compil[] = {%2$cgcc%2$c, %2$c-o%2$c, name_exec, name, NULL};%1$c%3$cchar *args_exec[] = {name_exec, NULL};%1$c%3$cif (!pid)%1$c%3$c%3$cexecve(%2$c/usr/bin/gcc%2$c, args_compil, NULL);%1$c%3$celse%1$c%3$c{%1$c%3$c%3$cwait(NULL);%1$c%3$c%3$cexecve(name_exec, args_exec, NULL);%1$c%3$c}%1$c%3$creturn (0);%1$c}%1$c%1$cint main(void)%1$c{%1$c%3$cint i = %4$d;%1$c%3$cchar *name;%1$c%3$cchar *name_exec;%1$c%1$c%3$cif ((!(name = get_name(i, 0))) || (!(name_exec = get_name(i, 1))))%1$c%3$c%3$creturn (1);%1$c%3$cif (i >= 0)%1$c%3$c{%1$c%3$c%3$cwrite_in_file(name, i - 1);%1$c%3$c%3$cif (fork_duplicate(name, name_exec, i - 1))%1$c%3$c%3$c%3$creturn (1);%1$c%3$c%3$cfree(name);%1$c%3$c%3$cfree(name_exec);%1$c%3$c}%1$c%3$creturn (0);%1$c}%1$c";
	dprintf(fd, str, 10, 34, 9, i, str);
	close(fd);
}

static int fork_duplicate(char *name, char *name_exec, int i)
{
	pid_t pid = fork();
	char *args_compil[] = {"gcc", "-o", name_exec, name, NULL};
	char *args_exec[] = {name_exec, NULL};
	if (!pid)
		execve("/usr/bin/gcc", args_compil, NULL);
	else
	{
		wait(NULL);
		execve(name_exec, args_exec, NULL);
	}
	return (0);
}

int main(void)
{
	int i = 5;
	char *name;
	char *name_exec;

	if ((!(name = get_name(i, 0))) || (!(name_exec = get_name(i, 1))))
		return (1);
	if (i >= 0)
	{
		write_in_file(name, i - 1);
		if (fork_duplicate(name, name_exec, i - 1))
			return (1);
		free(name);
		free(name_exec);
	}
	return (0);
}
